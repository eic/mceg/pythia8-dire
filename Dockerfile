FROM pythiadirebasis

# add python files and set environment variable
ADD plotting.py /usr/local/src
ADD py8settings.py /usr/local/src
ADD hepmcdummy.py /usr/local/src
ENV PYTHONPATH "$PYTHONPATH:/usr/local/src"

# add notebooks
RUN mkdir /usr/local/share/notebooks
ADD README /usr/local/share/notebooks
ADD pythiaPI.ipynb /usr/local/share/notebooks
ADD pythiaRivetPI.ipynb /usr/local/share/notebooks
ADD pythiaRivet.ipynb /usr/local/share/notebooks
ADD pythiaRivetUS.ipynb /usr/local/share/notebooks
ADD direEvent.ipynb /usr/local/share/notebooks
ADD direRivet.ipynb /usr/local/share/notebooks
ADD direRivetUS.ipynb /usr/local/share/notebooks
ADD tuning.ipynb /usr/local/share/notebooks

# add executable
ADD jupyterNotebooks /usr/local/bin
RUN chmod +x /usr/local/bin/jupyterNotebooks
ADD eventGeneratorRun /usr/local/bin
RUN chmod +x /usr/local/bin/eventGeneratorRun
ADD tuning /usr/local/bin
RUN chmod +x /usr/local/bin/tuning

ADD yoda-envelopes /usr/local/bin
RUN chmod +x /usr/local/bin/yoda-envelopes
ADD rivet-mkhtml-custom /usr/local/bin
RUN chmod +x /usr/local/bin/rivet-mkhtml-custom

# add template scripts
RUN mkdir /usr/local/share/templatescripts
ADD runMC.sh /usr/local/share/templatescripts
ADD runIpol.sh /usr/local/share/templatescripts
ADD runTune.sh /usr/local/share/templatescripts
ADD runBestTune.sh /usr/local/share/templatescripts

CMD /usr/local/bin/jupyterNotebooks
