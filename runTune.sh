#!/bin/bash

for i in {1..NRUNCOMBINATIONS}; do

  docker run --rm --privileged -u `id -u $USER` \
    -v $PWD:$PWD -w $PWD CONTAINER \
    prof2-tune -d RIVETDATADIR IPOLBASENAME$((i-1)).dat \
    --wfile WEIGHTSFILE --limits LIMITSFILE \
    -o TUNEBASENAME$((i-1)) > TUNEBASENAME$((i-1)).dat

done

