#!/bin/bash

echo

MINGOF=$(grep GOF TUNEBASENAME*/results.txt \
  | awk '{print $3}' \
  | awk 'NR==1{MIN = $1 + 0; next} \
         {if ($1 < MIN) MIN = $1;} \
         END {print MIN}')

MINGOFDIR=""
for i in {1..6}; do
  if [ $(grep $MINGOF TUNEBASENAME$((i-1))/results.txt | wc -l) == "1" ]; then
    MINGOFDIR=TUNEBASENAME$((i-1))
  fi
done

echo "Found smallest GOF of "$MINGOF" in "$MINGOFDIR

IPOLFILE=$(grep InterpolationFile $MINGOFDIR/results.txt | awk '{print $3}')

mkdir -p BESTTUNEDIR
mkdir -p BESTTUNEDIR/eigentunes

docker run --rm --privileged -u `id -u $USER` \
  -v $PWD:$PWD -w $PWD CONTAINER \
  prof2-eigentunes -d RIVETDATADIR -m $MINGOFDIR/results.txt \
  $IPOLFILE -o BESTTUNEDIR

PARAMNAMES=()
while read l; do
  if [[ $l = *"Tune"* ]]; then
    PARAMNAMES=($l)
  elif [[ $l = *"Central"* ]] || [[ $l = *"+"* ]] || [[ $l = *"-"* ]]; then
    ETDIR=BESTTUNEDIR/eigentunes/$(echo $l | awk '{print $1}')
    mkdir $ETDIR
    cp TEMPLATEFILE $ETDIR
    for idx in ${!PARAMNAMES[*]}; do
      if [[ $idx != "0" ]]; then
        PVAL=$(echo $l | awk -v var=$((idx+1)) '{print $var}')
        sed -i "s/{${PARAMNAMES[$idx]}}/$PVAL/g" $ETDIR/TEMPLATEFILE
      fi
    done
  fi
done <BESTTUNEDIR/*params

echo

for i in $(ls -d BESTTUNEDIR/eigentunes/{Central,*+,*-}); do
  docker run --rm -u `id -u $USER` \
    -v $PWD:$PWD -w $PWD CONTAINER \
    eventGeneratorRun dire $i/TEMPLATEFILE 1000 \
    $i/hepmcfifo $i/out.yoda \
    "ANALYSES"
done

echo

YODAFILES=""
CENTRALYODAFILE=$(ls BESTTUNEDIR/eigentunes/Central/*yoda)
for i in $(ls BESTTUNEDIR/eigentunes/{Central,*+,*-}/*yoda); do
  YODAFILES=$YODAFILES" "$i
done

docker run --rm -u `id -u $USER` \
  -v $PWD:$PWD -w $PWD CONTAINER \
  yoda-envelopes $YODAFILES -c $CENTRALYODAFILE \
  -o BESTTUNEDIR/eigentunes/envelope.yoda -m allErrors

docker run --rm -u `id -u $USER` \
  -v $PWD:$PWD -w $PWD CONTAINER \
  rivet-mkhtml-custom $PWD --mc-errs -o BESTTUNEDIR/eigentunes/rivet-plots \
  $CENTRALYODAFILE:"Title=central":"LineColor=red" \
  BESTTUNEDIR/eigentunes/envelope.yoda:"Title=envelope":"LineStyle=none":"ErrorBars=0":"ErrorBands=1":"ErrorBandColor=red":"ErrorBandOpacity=0.2"

echo

