#!/bin/bash

echo

# working directory
WORKDIR=$PWD

# build custom Rivet analyses
RIVETFILES=""
RIVETDATADIR="/usr/local/share/Rivet"
for ccfile in $(ls *.cc 2> /dev/null); do
  if [ $(grep -i "rivet" $ccfile | wc -l) != "0" ]; then
    echo "   Found custom Rivet analysis "$ccfile"."
    RIVETFILES=$RIVETFILES" "$ccfile
  fi
done
if [ "$RIVETFILES" == "" ]; then
  echo "   No custom Rivet analyses found!"
else
  echo "   Building library for custom Rivet analyses!"
  rivet-buildplugin RivetCustomAnalyses.so $RIVETFILES
  export RIVET_ANALYSIS_PATH=$RIVET_ANALYSIS_PATH:$MOUNTDIR
  export RIVET_REF_PATH=$RIVET_REF_PATH:$MOUNTDIR
  export RIVET_INFO_PATH=$RIVET_INFO_PATH:$MOUNTDIR
  export RIVET_DATA_PATH=$RIVET_DATA_PATH:$MOUNTDIR
  export RIVET_PLOT_PATH=$RIVET_PLOT_PATH:$MOUNTDIR
  echo "   Copying yoda files (assuming it is data) to"$RIVETDATADIR"."
  cp *yoda $RIVETDATADIR 2> /dev/null
fi

# export path for LHAPDF
export LHAPDF_DATA_PATH=$LHAPDF_DATA_PATH:$MOUNTDIR

# check number of arguments
if [[ "$#" -ne 1 && "$#" -ne 6 ]]; then
  echo "   Wrong number of command line arguments! Exiting!"
  echo
  exit 0
fi

GENERATOR=""
CMNDFILE=""
NUMEVENTS=""
HEPMCFILE=""
YODAFILE=""
RIVETANAS=""

# file is given
if [ "$#" -eq 1 ]; then
  RUNCLUSTER=$1
  # check if file exists
  if [ ! -f "$RUNCLUSTER" ]; then
    echo "   File "$RUNCLUSTER" does not exist! Exiting!"
    echo
    exit 0
  fi
  # check number of lines
  if [ $(wc -l < $RUNCLUSTER) != "6" ]; then
    echo "   Wrong number of lines in "$RUNCLUSTER"! Exiting!"
    echo
    exit 0
  fi
  # extract information
  GENERATOR=$(head -n 1 $RUNCLUSTER | tail -n 1)
  CMNDFILE=$(head -n 2 $RUNCLUSTER | tail -n 1)
  NUMEVENTS=$(head -n 3 $RUNCLUSTER | tail -n 1)
  HEPMCFILE=$(head -n 4 $RUNCLUSTER | tail -n 1)
  YODAFILE=$(head -n 5 $RUNCLUSTER | tail -n 1)
  RIVETANAS=$(head -n 6 $RUNCLUSTER | tail -n 1)
fi

# command line arguments are given
if [ "$#" -eq 6 ]; then
  # extract information
  GENERATOR=$1
  CMNDFILE=$2
  NUMEVENTS=$3
  HEPMCFILE=$4
  YODAFILE=$5
  RIVETANAS=$6
fi

# apply number of events
if [ "$GENERATOR" == "pythia" ]; then
  sed -i "/Main:numberOfEvents/ d" $CMNDFILE
  sed -i "1s/^/Main:numberOfEvents = ${NUMEVENTS}\n/" $CMNDFILE
fi

# extract Rivet analyses
IFS=', ' read -r -a array <<< "$RIVETANAS"
RIVETANAS=""
for analysis in "${array[@]}"; do
  RIVETANAS=$RIVETANAS" -a "$analysis
done

# prepare fifo
echo "   Calling mkfifo "$HEPMCFILE"!"
rm -fr $HEPMCFILE
mkfifo $HEPMCFILE

# start generator
if [ "$GENERATOR" == "dire" ]; then
  echo "   Starting Pythia/DIRE and Rivet!"
  dire --nevents $NUMEVENTS --input $CMNDFILE --hepmc_output $HEPMCFILE &
elif [ "$GENERATOR" == "pythia" ]; then
  echo "   Starting Pythia and Rivet!"
  pythiaHepMC $CMNDFILE $HEPMCFILE &
else
  echo "   Generator "$GENERATOR" is unknown! Exiting!"
  echo
  exit 0
fi

# start Rivet
#rivet $RIVETANAS -n $NUMEVENTS -H $YODAFILE $HEPMCFILE
rivet $RIVETANAS -H $YODAFILE $HEPMCFILE
# remove fifo
rm -fr $HEPMCFILE

echo
