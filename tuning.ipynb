{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tuning with Professor and Rivet"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This notebook shows how to use Professor, Rivet and Pythia 8 / DIRE for tuning. The tuning procedure starts by sampling a number of points in the parameter space. For all these points a Monte Carlo run is performed. The Monte Carlo response is interpolated with a Nth order polynomial for each bin of all observables. The actual tuning takes the interpolation results and minimizes the difference between data and interpolated Monte Carlo response. To obtain multiple results not all parameter points have to be included in the interpolation / tuning. Instead, several run combinations are build, for which the interpolation and tuning is performed.\n",
    "\n",
    "Note that this notebook provides a basic overview and is intended to be run only for a very small parameter space. The container includes the option to automatically generate scripts for running the steps, for instance, on a cluster. For more details see the documentation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import re, sys, os, subprocess, math\n",
    "from plotting import PDF\n",
    "rivet_data_directory = \"/usr/local/share/Rivet\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Input Files and Parameters"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As a first step, let's go through all input files and parameters that are required for the tuning. The names can be adjusted below."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "0) The directory, where the input files are stored."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "directory = \"..\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "1) A parameter file of the following form (note the missing colons, otherwise the template file below will not be processed correctly by Professor)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "parameter_file = \"ranges.dat\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"\n",
    "#PARAMETER     MIN  MAX\n",
    "StringPTsigma  0.2  0.7\n",
    "StringZaLund   0.5  2.0\n",
    "StringZbLund   0.0  2.0\n",
    "\"\"\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "2) A template file, to be used to produce the Monte Carlo runs, of the following form."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "template_file = \"template.cmnd\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"\n",
    "WeakBosonExchange:ff2ff(t:gmZ) = on\n",
    "Beams:idA                      = -11\n",
    "Beams:idB                      = 2212\n",
    "# set more Pythia 8 / DIRE parameters\n",
    "# parameters from tuning\n",
    "StringPT:sigma = {StringPTsigma}\n",
    "StringZ:aLund  = {StringZaLund}\n",
    "StringZ:bLund  = {StringZbLund}\n",
    "\"\"\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "3) A weights file, containing the weights of all observables you want to tune to, of the following form. Note that, when giving a single weight to two observables, the total weight of the observable with more bins is larger, as compared the observable with less bins."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "weights_file = \"weights.dat\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"\n",
    "/ANALYSIS_NAME/OBSERVABLE_NAME         1 # Set weight to   1 for each bin of this histo\n",
    "/ANALYSIS_NAME/OBSERVABLE_NAME       100 # Set weight to 100 for each bin of this histo\n",
    "/ANALYSIS_NAME/OBSERVABLE_NAME@0:20   10 # Set weight to  10 for bins with binEDGES in [0,20)\n",
    "/ANALYSIS_NAME/OBSERVABLE_NAME@20:40  50 # Set weight to  10 for bins with binEDGES in [20,50)\n",
    "/ANALYSIS_NAME/OBSERVABLE_NAME#0:20   10 # Set weight to  10 for bins with binINCIDES in [0,20)\n",
    "/ANALYSIS_NAME/OBSERVABLE_NAME#20:40  50 # Set weight to  10 for bins with binINDICES in [20,50)\n",
    "\"\"\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "4) A limits file for the actual tuning, to, for instance, set parameters to be fixed. Here we simply use the same file as for the sampling, such that there is no restriction. A limits file might have the following form."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "limits_file = \"ranges.dat\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"\n",
    "# Fixed\n",
    "StringPTsigma  0.33\n",
    "# Limits\n",
    "StringZaLund  0.7  1.8\n",
    "\"\"\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "5) The order of the polynomial for interpolating the Monte Carlo response."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "polynomial_order = \"3\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "6) The number of points to sample depends on the order of the polynomial $n$ and the number of parameters $P$. For stable tuning result you should use several times the minimum number of points. The number of coefficients of the polynomial (and minimum number of points) is given by\n",
    "$$N(n,P)=\\sum_{i=0}^n\\frac1{i!}\\prod_{j=0}^{i-1}(P+j)~.$$\n",
    "As an example, for a third-order polynomial, the minimum number of points is\n",
    "$$N(3,P)=1+P+\\frac1{2}P(P+1)+\\frac1{6}P(P+1)(P+2)~.$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def min_number_of_points(n, P):\n",
    "    result = 0\n",
    "    for i in range(n+1):\n",
    "        product = 1\n",
    "        for j in range(i): product *= (P+j)\n",
    "        result += product/math.factorial(i)\n",
    "    print result\n",
    "min_number_of_points(3, 8)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "number_of_points = \"5\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "7) Run combinations, one tune will be performed for each run combination. The syntax is a list of pairs A:B where A gives the number of parameter points to exclude and B the number of combinations. In the below example, one run combination with all parameter points and five run combinations with all but one parameter points are used."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "combinations   = \"0:1 1:5\"\n",
    "n_combinations = \"6\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Output Files"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Directory where the sampled parameters are stored.\n",
    "# They serve as the input for the Monte Carlo response,\n",
    "# to be stored in the same directory.\n",
    "output_directory = \"mc\"\n",
    "# Directory where the results for the best tune are stored.\n",
    "best_tune_directory = \"bestTune\"\n",
    "# File to store the run combinations.\n",
    "combinations_file = \"runcombs.dat\"\n",
    "# Basename for the interpolation output files.\n",
    "ipol_basename = \"interpolation/ipol\"\n",
    "# Basename for the tuning output files.\n",
    "tune_basename = \"tuning/tune\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Sampling"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following command produces a directory, given by output_directory, with subdirectories 0000 to number_of_points (with leading zeros). Each subdirectory contains two files: a file with the same name as the parameter file, with the parameters of the specific point, and a file with the same name as the template file, with replacements as specified in the template file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "subprocess.call([\"bash\",\"-c\",\"cd \"+directory+\" && prof2-sample -s 123456789\"+\n",
    "    \" -n \"+number_of_points+\" -t \"+template_file+\" -o \"+output_directory+\n",
    "    \" \"+parameter_file])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To remove the leading zeros in the subdirectories, exectute the following line."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "subprocess.call([\"bash\",\"-c\",\"cd \"+directory+\"/\"+output_directory+\" && for x in *; do a=${x:0:1}; b=${x:1:1}; c=${x:2:1}; d=${x:3:1}; out=\\\"\\\"; found=0;  if [[ \\\"$a\\\" != \\\"0\\\" ]] ; then found=1; out+=$a ; fi;  if [[ \\\"$b\\\" != \\\"0\\\" || \\\"$found\\\" == \\\"1\\\" ]] ; then found=1; out+=$b ; fi;  if [[ \\\"$c\\\" != \\\"0\\\" || \\\"$found\\\" == \\\"1\\\" ]] ; then found=1; out+=$c ; fi;  out+=$d ; echo \\\"mv $x $out\\\"; mv $x $out; done\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Run Combinations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The next step is to generate run combinations to allow for multiple tunes. One tune might contain all parameter points (0:1), and another ten tunes might contain contain all but one parameter points (1:10). You can add as many other combinations you want. The syntax is as follows: first entry is the number of parameter points to skip and the second entry the number of combinations to produce. The following command produces a file with one run combination per line."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "subprocess.call([\"bash\",\"-c\",\"cd \"+directory+\" && prof2-runcombs \"+output_directory+\n",
    "    \" \"+combinations+\" -o \"+combinations_file])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Monte Carlo Response"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now the Monte Carlo runs should be produced, for all parameter points that have just been sampled. Before starting the runs, the Rivet analyses are extracted from the weights file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open(directory+\"/\"+weights_file) as f:\n",
    "    weights = f.readlines()\n",
    "weights = [x for x in weights]\n",
    "rivet_analyses = \"\"\n",
    "for weight in weights:\n",
    "    if len(weight) > 0 and weight[0] == \"/\": weight = weight[1:]\n",
    "    if \"/\" in weight: weight = weight[:weight.find(\"/\")]\n",
    "    if \" \" in weight: weight = weight[:weight.find(\" \")]\n",
    "    if weight not in rivet_analyses: rivet_analyses += \" \"+weight\n",
    "if rivet_analyses != \"\": rivet_analyses = rivet_analyses[1:]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "number_of_events = \"1000\"\n",
    "generator        = \"dire\"\n",
    "yoda_file        = \"out.yoda\"\n",
    "hepmc_fifo       = \"hepmcfifo\"\n",
    "for i in range(int(number_of_points)):\n",
    "    mc_directory_now = output_directory+\"/\"+str(i)\n",
    "    subprocess.call([\"bash\",\"-c\",\"cd \"+directory+\" && \"+\n",
    "        \"eventGeneratorRun \"+generator+\" \"+\n",
    "        mc_directory_now+\"/\"+template_file+\" \"+\n",
    "        number_of_events+\" \"+\n",
    "        mc_directory_now+\"/\"+hepmc_fifo+\" \"+\n",
    "        mc_directory_now+\"/\"+yoda_file+\" \"+\n",
    "        \"\\\"\"+rivet_analyses+\"\\\"\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Interpolation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now the interpolation will be performed, one for each of the run combinations. The time each interpolation takes depends on the number of parameters, the order of the polynomial, and the number of observables."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for i in range(int(n_combinations)):\n",
    "    subprocess.call([\"bash\",\"-c\",\"cd \"+directory+\" && prof2-ipol \"+output_directory+\n",
    "        \" \"+ipol_basename+str(i)+\".dat --order \"+polynomial_order+\n",
    "        \" --wfile \"+weights_file+\" --rc \"+combinations_file+\":\"+str(i)])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Optionally: You can now check the parameterisation trustworthiness of the first interpolation with the following command. It will produce a few plots."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "subprocess.call([\"bash\",\"-c\",\"cd \"+directory+\" && prof2-residuals \"+output_directory+\n",
    "    \" \"+ipol_basename+\"0.dat\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Tuning"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now the actual tune can be performed, using the previously prepared files. The following command produces two files with a summary of minimized parameters and used weights, and an additional yoda file which is the result of the Monte Carlo interpolation at the minimized parameters. This can be useful for a quick of the tune check against data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for i in range(int(n_combinations)):\n",
    "    subprocess.call([\"bash\",\"-c\",\"cd \"+directory+\" && prof2-tune -d \"+rivet_data_directory+\n",
    "        \" \"+ipol_basename+str(i)+\".dat --wfile \"+weights_file+\n",
    "        \" --limits \"+limits_file+\" -o \"+tune_basename+str(i)+\n",
    "        \" > \"+tune_basename+str(i)+\".dat\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's now check the first tune by comparing the Monte Carlo interpolation at the minimized parameters to data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "subprocess.call([\"bash\",\"-c\",\"rivet-mkhtml --mc-errs --booklet \"+\n",
    "    directory+\"/\"+tune_basename+\"0/ipolhistos.yoda:\\\"Title=Interpolation\\\"\"])\n",
    "PDF(\"rivet-plots/booklet.pdf\",size=(600,500))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Finding the Best Tune"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As a last step we find the best tune of the ones we just produced. For this tune, the eigentunes are constructed, Monte Carlo runs are performed for the parameter points of the eigentunes, the envelope is constructed from the Monte Carlo runs, and finally the result is plotted."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# find the best tune\n",
    "min_gof     = sys.float_info.max\n",
    "min_gof_dir = \"\"\n",
    "for i in range(int(n_combinations)):\n",
    "    with open(directory+\"/\"+tune_basename+str(i)+\"/results.txt\") as f:\n",
    "        for line in f:\n",
    "            if len(re.findall(\"GOF\", line)) > 0:\n",
    "                gof = float(line.split()[2])\n",
    "                if gof < min_gof:\n",
    "                    min_gof     = gof\n",
    "                    min_gof_dir = tune_basename+str(i)\n",
    "min_ipol_file = \"\"\n",
    "with open(directory+\"/\"+min_gof_dir+\"/results.txt\") as f:\n",
    "    for line in f:\n",
    "        if len(re.findall(\"InterpolationFile\", line)) > 0:\n",
    "            min_ipol_file = line.split()[2]\n",
    "print \"Found smallest GOF of \"+str(min_gof)+\" in \"+min_gof_dir"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# produce the eigentunes\n",
    "if not os.path.exists(directory+\"/\"+best_tune_directory):\n",
    "    os.mkdir(directory+\"/\"+best_tune_directory)\n",
    "subprocess.call([\"bash\",\"-c\",\"cd \"+directory+\" && prof2-eigentunes -d \"+rivet_data_directory+\n",
    "    \" -m \"+min_gof_dir+\"/results.txt \"+min_ipol_file+\n",
    "    \" -o \"+best_tune_directory])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# prepare cmnd files\n",
    "eigentunes_directory = directory+\"/\"+best_tune_directory+\"/eigentunes\"\n",
    "if not os.path.exists(eigentunes_directory):\n",
    "    os.mkdir(eigentunes_directory)\n",
    "parameter_names        = []\n",
    "eigentunes_directories = []\n",
    "for ff in os.listdir(directory+\"/\"+best_tune_directory):\n",
    "    if ff.endswith(\".params\"):\n",
    "        with open(directory+\"/\"+best_tune_directory+\"/\"+ff) as f:\n",
    "            for line in f:\n",
    "                if \"Tune\" in line: parameter_names = line.split()\n",
    "                elif \"Central\" in line or \"+\" in line or \"-\" in line:\n",
    "                    eigentunes_directories.append(best_tune_directory+\"/eigentunes/\"+line.split()[0])\n",
    "                    if not os.path.exists(eigentunes_directory+\"/\"+line.split()[0]):\n",
    "                        os.mkdir(eigentunes_directory+\"/\"+line.split()[0])\n",
    "                    with open(directory+\"/\"+template_file, \"rt\") as fin:\n",
    "                        with open(eigentunes_directory+\"/\"+line.split()[0]+\"/\"+template_file, \"wt\") as fout:\n",
    "                            for cmndline in fin:\n",
    "                                for i in range(1,len(parameter_names)):\n",
    "                                    fout.write(cmndline.replace(\"{\"+parameter_names[i]+\"}\", line.split()[1]))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# perform Monte Carlo runs for eigentune parameter points\n",
    "number_of_events = \"1000\"\n",
    "generator        = \"dire\"\n",
    "yoda_file        = \"out.yoda\"\n",
    "hepmc_fifo       = \"hepmcfifo\"\n",
    "for eigentunes_directory in eigentunes_directories:\n",
    "    subprocess.call([\"bash\",\"-c\",\"cd \"+directory+\" && \"+\n",
    "        \"eventGeneratorRun \"+generator+\" \"+\n",
    "        eigentunes_directory+\"/\"+template_file+\" \"+\n",
    "        number_of_events+\" \"+\n",
    "        eigentunes_directory+\"/\"+hepmc_fifo+\" \"+\n",
    "        eigentunes_directory+\"/\"+yoda_file+\" \"+\n",
    "        \"\\\"\"+rivet_analyses+\"\\\"\"])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# produce envelope and plot\n",
    "central_yoda_file = eigentunes_directories[0]+\"/\"+yoda_file\n",
    "yoda_files = \"\"\n",
    "for eigentunes_directory in eigentunes_directories:\n",
    "    yoda_files += \" \"+eigentunes_directory+\"/\"+yoda_file\n",
    "subprocess.call([\"bash\",\"-c\",\"cd \"+directory+\" && yoda-envelopes\"+\n",
    "    yoda_files+\" -c \"+central_yoda_file+\" -o \"+\n",
    "    best_tune_directory+\"/eigentunes/envelope.yoda -m allErrors\"])\n",
    "subprocess.call([\"bash\",\"-c\",\"rivet-mkhtml --mc-errs --booklet \"+\n",
    "    directory+\"/\"+eigentunes_directories[0]+\"/\"+yoda_file+\n",
    "    \":\\\"Title=central\\\":\\\"LineColor=red\\\" \"+\n",
    "    directory+\"/\"+best_tune_directory+\"/eigentunes/envelope.yoda\"+\n",
    "    \":\\\"Title=envelope\\\":\\\"LineStyle=none\\\":\\\"ErrorBars=0\\\":\\\"ErrorBands=1\\\"\"+\n",
    "    \":\\\"ErrorBandColor=red\\\":\\\"ErrorBandOpacity=0.2\\\"\"])\n",
    "PDF(\"rivet-plots/booklet.pdf\",size=(600,500))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That's it. Now you know the basics!"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
