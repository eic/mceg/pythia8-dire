#!/bin/bash

for i in {1..NPOINTS}; do

  DIR=MCDIR/$((i-1))
  docker run --rm -u `id -u $USER` \
    -v $PWD:$PWD -w $PWD CONTAINER \
    eventGeneratorRun dire $DIR/TEMPLATEFILE 1000 \
    $DIR/hepmcfifo $DIR/out.yoda \
    "ANALYSES"

done

