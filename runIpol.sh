#!/bin/bash

for i in {1..NRUNCOMBINATIONS}; do

  docker run --rm --privileged -u `id -u $USER` \
    -v $PWD:$PWD -w $PWD CONTAINER \
    prof2-ipol MCDIR IPOLBASENAME$((i-1)).dat \
    --order POLYORDER --wfile WEIGHTSFILE \
    --rc COMBINATIONSFILE:$((i-1))

done

